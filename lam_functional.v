Require Import List.
Import ListNotations.

Inductive typ: Set :=
    | Ty_1: typ
    | Ty_arrow: typ -> typ -> typ.

Notation "A ⇒ B" := (Ty_arrow A B) (at level 50).

Definition ctx: Set := list typ.

Reserved Notation "A ∈ Γ" (at level 70).

Inductive var (A: typ): ctx -> Set :=
    | var_z Γ: A ∈ A :: Γ
    | var_s B Γ: A ∈ Γ -> A ∈ B :: Γ

    where "A ∈ Γ" := (var A Γ).

Reserved Notation "Γ ⊢ A" (at level 80).

Inductive tm: ctx -> typ -> Set :=
    | tm_1 Γ: Γ ⊢ Ty_1
    | tm_var Γ A: A ∈ Γ -> Γ ⊢ A
    | tm_lam Γ A B: A :: Γ ⊢ B -> Γ ⊢ A ⇒ B
    | tm_app Γ A B: Γ ⊢ A ⇒ B -> Γ ⊢ A -> Γ ⊢ B

    where "Γ ⊢ A" := (tm Γ A).

Notation "Γ ⊑ Δ" := (forall A, A ∈ Δ -> A ∈ Γ) (at level 80).

Fixpoint wk_drop Γ A: A :: Γ ⊑ Γ.
Proof.
    destruct Γ.
    - intros B v. inversion v.
    - intros B v. apply var_s. apply v.
Defined.

Fixpoint shift_wk Γ Δ A: Γ ⊑ Δ -> A :: Γ ⊑ A :: Δ.
Proof.
    intros F B v.
    inversion v as [|C v']; subst.
    - apply var_z.
    - apply var_s. apply F. apply H.
Qed.

Fixpoint wk_tm Γ Δ A (F: Γ ⊑ Δ) (t: Δ ⊢ A): Γ ⊢ A.
Proof.
    inversion t as [| | Γ' A' B s | Γ' A' B f x]; subst.
    - apply tm_1.
    - apply tm_var. apply F. apply H.
    - apply tm_lam. eapply wk_tm. apply shift_wk. apply F. apply s.
    - eapply tm_app; eapply wk_tm; eassumption.
Defined.

Definition ctx_sub Γ Δ := forall A, A ∈ Δ -> Γ ⊢ A.

Definition wk_sub Γ Δ Θ (w: Γ ⊑ Δ) (σ: ctx_sub Δ Θ): ctx_sub Γ Θ
    := fun A x => wk_tm _ _ _ w (σ A x).

Definition shift_sub Γ Δ A (σ: ctx_sub Γ Δ): ctx_sub (A :: Γ) (A :: Δ).
Proof.
    intros B t.
    inversion t as [| C Γ']; subst.
    - apply tm_var. apply var_z.
    - eapply wk_tm. apply wk_drop. apply σ. apply H.
Defined. 

Fixpoint sub_tm Γ Δ A (σ: ctx_sub Γ Δ) (t: Δ ⊢ A): Γ ⊢ A.
Proof.
    inversion t as [| Γ' A' v | Γ' A' B s | Γ' A' B f x]; subst.
    - apply tm_1.
    - apply σ. apply v.
    - apply tm_lam. eapply sub_tm. 
      * apply shift_sub. apply σ.
      * apply s.
    - eapply tm_app; eapply sub_tm; try apply σ; eassumption.
Defined.