Require Import List.
Import ListNotations.

Inductive typ: Set :=
    | Ty_1: typ
    | Ty_arrow: typ -> typ -> typ.

Notation "A ⇒ B" := (Ty_arrow A B) (at level 50).

Definition ctx: Set := list typ.

Reserved Notation "A ∈ Γ" (at level 70).

Inductive var (A: typ): ctx -> Set :=
    | var_z Γ: A ∈ A :: Γ
    | var_s B Γ: A ∈ Γ -> A ∈ B :: Γ

    where "A ∈ Γ" := (var A Γ).

Reserved Notation "Γ ⊢ A" (at level 80).

Inductive tm: ctx -> typ -> Set :=
    | tm_1 Γ: Γ ⊢ Ty_1
    | tm_var Γ A: A ∈ Γ -> Γ ⊢ A
    | tm_lam Γ A B: A :: Γ ⊢ B -> Γ ⊢ A ⇒ B
    | tm_app Γ A B: Γ ⊢ A ⇒ B -> Γ ⊢ A -> Γ ⊢ B

    where "Γ ⊢ A" := (tm Γ A).

Reserved Notation "Γ ⊑ Δ" (at level 80).

Inductive wk_ctx: ctx -> ctx -> Set :=
    | wk_z: [] ⊑ []
    | wk_s Γ Δ A: Γ ⊑ Δ -> (A :: Γ) ⊑ (A :: Δ)
    | wk_d Γ Δ A: Γ ⊑ Δ -> (A :: Γ) ⊑ Δ

    where "Γ ⊑ Δ" := (wk_ctx Γ Δ).

Fixpoint wk_var Γ Δ A (H: Γ ⊑ Δ) (v: A ∈ Δ): A ∈ Γ.
Proof.
    destruct H as [| Γ Δ B | Γ Δ B].
    - inversion v.
    - inversion v; subst.
      * apply var_z.
      * apply var_s. apply (wk_var Γ Δ). apply H. apply H1.
    - apply var_s. apply (wk_var Γ Δ). apply H. apply v.
Defined.

Fixpoint wk_id Γ: Γ ⊑ Γ.
Proof.
    destruct Γ.
    - apply wk_z.
    - apply wk_s. apply wk_id.
Defined.

Fixpoint wk_tm Γ Δ A (H: Γ ⊑ Δ) (t: Δ ⊢ A): Γ ⊢ A.
Proof.
    destruct t.
    - apply tm_1.
    - apply tm_var. eapply wk_var. apply H. apply v.
    - apply tm_lam. eapply wk_tm. apply wk_s. apply H. apply t.
    - eapply tm_app.
      * eapply wk_tm. apply H. apply t1.
      * eapply wk_tm. apply H. apply t2.
Defined.

Inductive ctx_sub (Γ: ctx): ctx -> Set :=
    | sub_nil: ctx_sub Γ []
    | sub_cons Δ A: Γ ⊢ A -> ctx_sub Γ Δ -> ctx_sub Γ (A :: Δ).

Fixpoint wk_sub Γ Δ Θ (H: Γ ⊑ Δ) (S: ctx_sub Δ Θ): ctx_sub Γ Θ.
Proof.
    inversion S as [| Δ' B]; subst.
    - apply sub_nil.
    - apply sub_cons. 
      * eapply wk_tm. apply H. apply H0. 
      * eapply wk_sub. apply H. apply H1.
Defined.

Fixpoint sub_var Γ Δ A (S: ctx_sub Γ Δ) (v: A ∈ Δ): Γ ⊢ A.
Proof.
    inversion S as [|Δ' A' t S']; subst.
    - inversion v.
    - inversion v as [|B Γ' v']; subst.
      * apply t.
      * eapply sub_var. apply S'. apply v'.
Defined.

Fixpoint sub_tm Γ Δ A (S: ctx_sub Γ Δ) (t: Δ ⊢ A): Γ ⊢ A.
Proof.
    inversion t as [| Γ' A' v | Γ' A' B s | Γ' A' B f x ]; subst.
    - apply tm_1.
    - eapply sub_var. apply S. apply v.
    - apply tm_lam. eapply sub_tm. 
      * apply sub_cons.
        + apply tm_var. apply var_z.
        + eapply wk_sub.
          eapply wk_d.
          apply wk_id.
          apply S.  
      * apply s.
    - eapply tm_app.
      * eapply sub_tm. apply S. apply f.
      * eapply sub_tm. apply S. apply x.
Defined.